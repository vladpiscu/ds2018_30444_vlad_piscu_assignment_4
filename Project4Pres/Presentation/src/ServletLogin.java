import service.ServiceFactory;
import service.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class ServletLogin extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("uname");
        String password = request.getParameter("psw");
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        User user;
        switch (action) {
            case "Login":
                user = ServiceFactory.instance()
                        .getLoginService()
                        .login(username, password);
                if (user == null) {
                    RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.html");
                    requestDispatcher.forward(request, response);
                } else {
                    session.setAttribute("userId", user.getId());
                    if (user.getRole() == Role.ADMIN) {
                        session.setAttribute("role", "admin");
                        response.sendRedirect("/adminPage");
                    } else {
                        session.setAttribute("role", "client");
                        response.sendRedirect("/userPage");
                    }
                }
                break;
            case "Register":
                user = ServiceFactory.instance()
                        .getLoginService()
                        .register(username, password);
                if (user == null) {
                    RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.html");
                    requestDispatcher.forward(request, response);
                } else {
                        session.setAttribute("role", "client");
                        response.sendRedirect("/userPage");
                }
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiceFactory.instance();
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.html");
        requestDispatcher.forward(request, response);
    }
}
