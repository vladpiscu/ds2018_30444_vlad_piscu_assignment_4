
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAllPackagesByUserResult" type="{http://service/}ArrayOfPackage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllPackagesByUserResult"
})
@XmlRootElement(name = "GetAllPackagesByUserResponse")
public class GetAllPackagesByUserResponse {

    @XmlElement(name = "GetAllPackagesByUserResult")
    protected ArrayOfPackage getAllPackagesByUserResult;

    /**
     * Gets the value of the getAllPackagesByUserResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPackage }
     *     
     */
    public ArrayOfPackage getGetAllPackagesByUserResult() {
        return getAllPackagesByUserResult;
    }

    /**
     * Sets the value of the getAllPackagesByUserResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPackage }
     *     
     */
    public void setGetAllPackagesByUserResult(ArrayOfPackage value) {
        this.getAllPackagesByUserResult = value;
    }

}
