
package service;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RegisterResponse }
     * 
     */
    public RegisterResponse createRegisterResponse() {
        return new RegisterResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link SearchPackage }
     * 
     */
    public SearchPackage createSearchPackage() {
        return new SearchPackage();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link Register }
     * 
     */
    public Register createRegister() {
        return new Register();
    }

    /**
     * Create an instance of {@link GetRouteForPackage }
     * 
     */
    public GetRouteForPackage createGetRouteForPackage() {
        return new GetRouteForPackage();
    }

    /**
     * Create an instance of {@link GetRouteForPackageResponse }
     * 
     */
    public GetRouteForPackageResponse createGetRouteForPackageResponse() {
        return new GetRouteForPackageResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRouteEntry }
     * 
     */
    public ArrayOfRouteEntry createArrayOfRouteEntry() {
        return new ArrayOfRouteEntry();
    }

    /**
     * Create an instance of {@link GetAllCitiesResponse }
     * 
     */
    public GetAllCitiesResponse createGetAllCitiesResponse() {
        return new GetAllCitiesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfCity }
     * 
     */
    public ArrayOfCity createArrayOfCity() {
        return new ArrayOfCity();
    }

    /**
     * Create an instance of {@link GetAllPackagesByUser }
     * 
     */
    public GetAllPackagesByUser createGetAllPackagesByUser() {
        return new GetAllPackagesByUser();
    }

    /**
     * Create an instance of {@link SearchPackageResponse }
     * 
     */
    public SearchPackageResponse createSearchPackageResponse() {
        return new SearchPackageResponse();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link GetAllPackagesByUserResponse }
     * 
     */
    public GetAllPackagesByUserResponse createGetAllPackagesByUserResponse() {
        return new GetAllPackagesByUserResponse();
    }

    /**
     * Create an instance of {@link ArrayOfPackage }
     * 
     */
    public ArrayOfPackage createArrayOfPackage() {
        return new ArrayOfPackage();
    }

    /**
     * Create an instance of {@link GetAllCities }
     * 
     */
    public GetAllCities createGetAllCities() {
        return new GetAllCities();
    }

    /**
     * Create an instance of {@link RouteEntry }
     * 
     */
    public RouteEntry createRouteEntry() {
        return new RouteEntry();
    }

    /**
     * Create an instance of {@link City }
     * 
     */
    public City createCity() {
        return new City();
    }

}
