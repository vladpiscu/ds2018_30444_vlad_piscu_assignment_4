package service;

import entity.City;
import entity.Package;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IRouteService {
    @WebMethod
    Package trackPackage(int packageId);

    @WebMethod
    boolean updatePackageRoute(City city, String time, int packageId);
}
