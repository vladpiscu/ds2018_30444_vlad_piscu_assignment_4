
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetRouteForPackageResult" type="{http://service/}ArrayOfRouteEntry" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getRouteForPackageResult"
})
@XmlRootElement(name = "GetRouteForPackageResponse")
public class GetRouteForPackageResponse {

    @XmlElement(name = "GetRouteForPackageResult")
    protected ArrayOfRouteEntry getRouteForPackageResult;

    /**
     * Gets the value of the getRouteForPackageResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRouteEntry }
     *     
     */
    public ArrayOfRouteEntry getGetRouteForPackageResult() {
        return getRouteForPackageResult;
    }

    /**
     * Sets the value of the getRouteForPackageResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRouteEntry }
     *     
     */
    public void setGetRouteForPackageResult(ArrayOfRouteEntry value) {
        this.getRouteForPackageResult = value;
    }

}
