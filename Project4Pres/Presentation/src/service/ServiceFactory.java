package service;

import entity.City;
import entity.User;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class ServiceFactory {
    private IPackageService packageService;
    private LoginServiceSoap loginService;

    private static ServiceFactory serviceFactory;

    private ServiceFactory(){
        URL url = null;
        try {
            //1st argument serviceFirst URI, refer to wsdl document above
            //2nd argument is serviceFirst name, refer to wsdl document above
            url = new URL("http://localhost:7779/ws/packageService?wsdl");
            QName qname = new QName("http://service/", "PackageServiceService");
            Service service = Service.create(url, qname);
            packageService = service.getPort(IPackageService.class);
            /*
            url = new URL("http://localhost:2993/LoginService.asmx?wsdl");
            qname = new QName("http://service/", "LoginService");
            service = Service.create(url, qname);
            qname = new QName("http://service/", "LoginServiceSoap12");
            */
            LoginService loginServiceService = new LoginService();
            loginService = loginServiceService.getLoginServiceSoap();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }

    public static void main(String[] args) {
        LoginService loginService = new LoginService();


        service.User user1 = loginService.getLoginServiceSoap().login("a", "a");
        List<service.City> cities1 = loginService.getLoginServiceSoap().getAllCities().getCity();
        List<service.RouteEntry> route = loginService.getLoginServiceSoap().getRouteForPackage(2).getRouteEntry();
        return;
    }

    public static ServiceFactory instance(){
        if(serviceFactory == null)
            serviceFactory = new ServiceFactory();
        return serviceFactory;
    }

    public IPackageService getPackageService() {
        return packageService;
    }

    public LoginServiceSoap getLoginService() {
        return loginService;
    }
}
