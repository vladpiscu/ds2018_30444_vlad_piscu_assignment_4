package service;

import entity.City;
import entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(name = "LoginServiceSoap12")
public interface ILoginService {
    @WebMethod(operationName = "Login")
    User login(String username, String password);

    @WebMethod(operationName = "Register")
    User register(String username, String password);

    @WebMethod(operationName = "GetAllCities")
    List<City> getAllCities();
}
