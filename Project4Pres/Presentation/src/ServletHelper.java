import entity.User;
import service.City;
import service.Package;
import service.RouteEntry;

import java.io.PrintWriter;
import java.util.List;

public class ServletHelper {

    public static void printUserTable(List<User> users, PrintWriter out) {
        out.println("<table>");
        out.println("<body>");
        out.println("<thead>");
        out.println("<tr>");
        out.println("<th>Id</th>");
        out.println("<th>Username</th>");
        out.println("</tr>");
        out.println("</thead>");
        for(User user : users){
            out.println("<tr>");
            out.println("<th>" + user.getId() + "</th>");
            out.println("<th>" + user.getUsername() + "</th>");
            out.println("</tr>");
        }
        out.println("<tbody>");
        out.println("</tbody>");
        out.println("</table>");
    }

    public static void printPackageIdTable(List<Integer> packagesId, PrintWriter out) {
        out.println("<table>");
        out.println("<body>");
        out.println("<thead>");
        out.println("<tr>");
        out.println("<th>Id</th>");
        out.println("</tr>");
        out.println("</thead>");
        for(Integer packageId : packagesId){
            out.println("<tr>");
            out.println("<th>" + packageId + "</th>");
            out.println("</tr>");
        }
        out.println("<tbody>");
        out.println("</tbody>");
        out.println("</table>");
    }

    public static void printPackageTable(List<Package> packages, PrintWriter out) {
        out.println("<table>");
        out.println("<body>");
        out.println("<thead>");
        out.println("<tr>");
        out.println("<th>Id</th>");
        out.println("<th>Sender</th>");
        out.println("<th>Receiver</th>");
        out.println("<th>Name</th>");
        out.println("<th>Description</th>");
        out.println("<th>Sender City</th>");
        out.println("<th>Receiver City</th>");
        out.println("</tr>");
        out.println("</thead>");
        for(Package aPackage : packages){
            out.println("<tr>");
            out.println("<th>" + aPackage.getId() + "</th>");
            out.println("<th>" + aPackage.getSender().getUsername() + "</th>");
            out.println("<th>" + aPackage.getReceiver().getUsername() + "</th>");
            out.println("<th>" + aPackage.getName() + "</th>");
            out.println("<th>" + aPackage.getDescription() + "</th>");
            out.println("<th>" + aPackage.getSenderCity().getName() + "</th>");
            out.println("<th>" + aPackage.getReceiverCity().getName() + "</th>");
            out.println("</tr>");
        }
        out.println("<tbody>");
        out.println("</tbody>");
        out.println("</table>");
    }

    public static void printRouteTable(List<RouteEntry> routeEntries, PrintWriter out){
        if(routeEntries != null) {
            out.println("<table>");
            out.println("<body>");
            out.println("<thead>");
            out.println("<tr>");
            out.println("<th>City name</th>");
            out.println("<th>Time</th>");
            out.println("</tr>");
            out.println("</thead>");
            for (RouteEntry routeEntry : routeEntries) {
                out.println("<tr>");
                out.println("<th>" + routeEntry.getCity().getName() + "</th>");
                out.println("<th>" + routeEntry.getTime().toString() + "</th>");
                out.println("</tr>");
            }
            out.println("<tbody>");
            out.println("</tbody>");
            out.println("</table>");
        }
    }

    public static void printCitiesTable(List<City> cities, PrintWriter out) {
        out.println("<table>");
        out.println("<body>");
        out.println("<thead>");
        out.println("<tr>");
        out.println("<th>Id</th>");
        out.println("<th>Name</th>");
        out.println("</tr>");
        out.println("</thead>");
        for(City city : cities){
            out.println("<tr>");
            out.println("<th>" + city.getId() + "</th>");
            out.println("<th>" + city.getName() + "</th>");
            out.println("</tr>");
        }
        out.println("<tbody>");
        out.println("</tbody>");
        out.println("</table>");
    }

}
