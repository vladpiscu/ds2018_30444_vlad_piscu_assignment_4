import service.Package;
import service.RouteEntry;
import service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ServletGetRoute")
public class ServletGetRoute extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(!request.getParameter("id").equals("")) {
            int packageId = Integer.parseInt(request.getParameter("id"));
            int userId =  (int) request.getSession().getAttribute("userId");
            List<Package> packages = ServiceFactory.instance().getLoginService().getAllPackagesByUser(userId).getPackage();
            List<RouteEntry> routeEntries = ServiceFactory.instance().getLoginService().getRouteForPackage(packageId).getRouteEntry();
            try (PrintWriter out = response.getWriter()) {
                printPage(out, packages, routeEntries);
            }
        }
        else{
            int userId =  (int) request.getSession().getAttribute("userId");
            List<Package> packages = ServiceFactory.instance().getLoginService().getAllPackagesByUser(userId).getPackage();
            try (PrintWriter out = response.getWriter()) {
                printPage(out, packages, null);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int userId =  (int) request.getSession().getAttribute("userId");
        List<Package> packages = ServiceFactory.instance().getLoginService().getAllPackagesByUser(userId).getPackage();
        try (PrintWriter out = response.getWriter()) {
            printPage(out, packages, null);
        }
    }

    private void printPage(PrintWriter out, List<Package> packages, List<RouteEntry> routeEntries){
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Crud packages</title>");
        out.println("<style>");
        out.println("td { width: 200px; }");
        out.println("th { text-align: left; }");
        out.println("table, th, td { border-collapse: collapse; border: 1px solid black; }");
        out.println("</style>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>All Packages</h1>");
        ServletHelper.printPackageTable(packages, out);
        out.println("</br>");
        out.println("<h1>Route</h1>");
        ServletHelper.printRouteTable(routeEntries, out);
        out.println("</br>");
        printPackageForm(out);
        out.println("<a href=\"userPage\"><b>Back</b></a>");
        out.println("</body>");
        out.println("</html>");
    }

    private void printPackageForm(PrintWriter out){
        out.println("<hr/>");
        out.println("<form method=\"post\">");
        out.println("Package id: <input type=\"number\" name=\"id\"/>");
        out.println("<p>");
        out.println("<input type=\"submit\" class=\"button\" name=\"action\" value=\"Show route\" />");
        out.println("</form>");
    }
}
