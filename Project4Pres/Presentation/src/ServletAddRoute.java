import service.City;
import service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "ServletAddRoute")
public class ServletAddRoute extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        List<City> cities = ServiceFactory.instance().getLoginService().getAllCities().getCity();
        List<Integer> packageIds = ServiceFactory.instance().getPackageService().getAllPackages().stream().map(p -> p.getId()).collect(Collectors.toList());
        int id = Integer.parseInt(request.getParameter("searchCity"));
        int packageId = Integer.parseInt(request.getParameter("searchPackage"));
        entity.City city = ServiceFactory.instance().getPackageService().getCityById(id);
        LocalDateTime localDateTime = LocalDateTime.parse(request.getParameter("arrivalTime"));
        boolean packageIsTracked = ServiceFactory.instance().getPackageService().updatePackageRoute(city, Timestamp.valueOf(localDateTime).toString(), packageId);

        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Add route</title>");
            out.println("<style>");
            out.println("td { width: 200px; }");
            out.println("th { text-align: left; }");
            out.println("table, th, td { border-collapse: collapse; border: 1px solid black; }");
            out.println("</style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>All Packages</h1>");
            ServletHelper.printPackageIdTable(packageIds, out);
            out.println("<h1>All Cities</h1>");
            ServletHelper.printCitiesTable(cities, out);
            printCityForm(out);
            if(!packageIsTracked)
                out.println("<b>The package is not tracked!</b>");
            out.println("<a href=\"/userPage\"><b>Back</b></a>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        List<City> cities = ServiceFactory.instance().getLoginService().getAllCities().getCity();
        List<Integer> packageIds = ServiceFactory.instance().getPackageService().getAllPackages().stream().map(p -> p.getId()).collect(Collectors.toList());
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Add route</title>");
            out.println("<style>");
            out.println("td { width: 200px; }");
            out.println("th { text-align: left; }");
            out.println("table, th, td { border-collapse: collapse; border: 1px solid black; }");
            out.println("</style>");
            out.println("</head>");
            out.println("<body>");
            out.println("</br>");
            out.println("<h1>All Packages</h1>");
            ServletHelper.printPackageIdTable(packageIds, out);
            out.println("<h1>All Cities</h1>");
            ServletHelper.printCitiesTable(cities, out);
            printCityForm(out);
            out.println("<a href=\"/adminPage\"><b>Back</b></a>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    private void printCityForm(PrintWriter out) {
        out.println("<hr/>");
        out.println("<form method=\"post\">");
        out.println("<legend><b>Add city to the route:</b></legend>");
        out.println("Package id: <input type=\"number\" name=\"searchPackage\" required/>");
        out.println("City id: <input type=\"number\" name=\"searchCity\" required/>");
        out.println("Time: <input type=\"datetime-local\" name=\"arrivalTime\" required/>");
        out.println("<p>");
        out.println("<input type=\"submit\" class=\"button\" name=\"action\" value=\"Add entry\" />");
        out.println("</form>");
    }


}