import entity.Package;
import entity.User;
import service.City;
import service.IPackageService;
import service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "ServletCrudPackage")
public class ServletCrudPackage extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idValue = request.getParameter("id");
        int id, departureCityId, arrivalCityId, senderId, receiverId;
        boolean creationError = false;
        boolean trackError = false;
        boolean deleteError = false;
        if(idValue.equals("")) {
            id = -1;
            trackError = true;
            deleteError = true;
        }
        else
            id = Integer.parseInt(idValue);
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String senderUserValue = request.getParameter("senderId");
        if(senderUserValue.equals("")) {
            senderId = -1;
            creationError = true;
        }
        else
            senderId = Integer.parseInt(senderUserValue);
        String receiverUserValue = request.getParameter("receiverId");
        if(receiverUserValue.equals("")) {
            receiverId = -1;
            creationError = true;
        }
        else
            receiverId = Integer.parseInt(receiverUserValue);
        String departureCityValue = request.getParameter("senderCityId");
        if(departureCityValue.equals("")) {
            departureCityId = -1;
            creationError = true;
        }
        else
            departureCityId = Integer.parseInt(departureCityValue);
        String arrivalCityValue = request.getParameter("receiverCityId");
        if(arrivalCityValue.equals("")) {
            arrivalCityId = -1;
            creationError = true;
        }
        else
            arrivalCityId = Integer.parseInt(arrivalCityValue);
        String action = request.getParameter("action");
        IPackageService packageService = ServiceFactory.instance().getPackageService();
        entity.City departureCity, arrivalCity;
        User senderUser, receiverUser;
        String error = "";
        switch (action){
            case "Create":
                if(creationError)
                    error = "ERROR! You should complete every field apart from the id!";
                departureCity = packageService.getCityById(departureCityId);
                arrivalCity = packageService.getCityById(arrivalCityId);
                senderUser = packageService.getUserById(senderId);
                receiverUser = packageService.getUserById(receiverId);
                if(departureCity == null || arrivalCity == null || senderUser == null || receiverUser == null)
                    error = "ERROR! You should complete every field with values that exist!";
                else
                    packageService.addPackage(new Package(senderUser, receiverUser, name, description, departureCity, arrivalCity, false));
                break;
            case "Track":
                if(trackError)
                    error = "ERROR! You should complete the package id!";
                packageService.trackPackage(id);
                break;
            case "Delete":
                if(deleteError)
                    error = "ERROR! You should complete the package id!";
                packageService.removePackage(id);
                break;
        }
        printPage(response, error);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        printPage(response, "");
    }

    private void printPage(HttpServletResponse response, String error) throws IOException{
        response.setContentType("text/html;charset=UTF-8");
        List<User> users = ServiceFactory.instance().getPackageService().getAllUsers();
        List<City> cities = ServiceFactory.instance().getLoginService().getAllCities().getCity();
        List<Integer> packageIds = ServiceFactory.instance().getPackageService().getAllPackages().stream().map(p -> p.getId()).collect(Collectors.toList());

        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Crud packages</title>");
            out.println("<style>");
            out.println("td { width: 200px; }");
            out.println("th { text-align: left; }");
            out.println("table, th, td { border-collapse: collapse; border: 1px solid black; }");
            out.println("</style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>All Packages</h1>");
            ServletHelper.printPackageIdTable(packageIds, out);
            out.println("<h1>All users</h1>");
            ServletHelper.printUserTable(users, out);
            out.println("</br>");
            out.println("<h1>All cities</h1>");
            ServletHelper.printCitiesTable(cities, out);
            out.println("</br>");
            printPackageForm(out);
            out.println("<p><b>" + error + "</b></p>");
            out.println("<a href=\"/adminPage\"><b>Back</b></a>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    private void printPackageForm(PrintWriter out) {
        out.println("<hr/>");
        out.println("<form method=\"post\">");
        out.println("<legend><b>Make operations on packages:</b></legend>");
        out.println("Id: <input type=\"number\" name=\"id\"/>");
        out.println("<p>");
        out.println("Sender id: <input type=\"number\" name=\"senderId\"/>");
        out.println("<p>");
        out.println("Receiver id: <input type=\"number\" name=\"receiverId\"/>");
        out.println("<p>");
        out.println("Name: <input type=\"text\" name=\"name\"/>");
        out.println("<p>");
        out.println("Description: <input type=\"text\" name=\"description\"/>");
        out.println("<p>");
        out.println("Sender city id: <input type=\"number\" name=\"senderCityId\"/>");
        out.println("<p>");
        out.println("Receiver city id: <input type=\"number\" name=\"receiverCityId\"/>");
        out.println("<p>");
        out.println("<input type=\"submit\" class=\"button\" name=\"action\" value=\"Create\" />");
        out.println("<input type=\"submit\" class=\"button\" name=\"action\" value=\"Delete\" />");
        out.println("<input type=\"submit\" class=\"button\" name=\"action\" value=\"Track\" />");
        out.println("</form>");
    }
}
