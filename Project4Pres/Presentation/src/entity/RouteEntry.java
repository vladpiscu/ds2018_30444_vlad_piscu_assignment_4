package entity;

import java.sql.Timestamp;

public class RouteEntry {
    private int id;

    private City city;
    private Timestamp time;
    private Package aPackage;

    public RouteEntry() {
    }

    public RouteEntry(City city, Timestamp time, Package aPackage) {
        this.city = city;
        this.time = time;
        this.aPackage = aPackage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public Package getaPackage() {
        return aPackage;
    }

    public void setaPackage(Package aPackage) {
        this.aPackage = aPackage;
    }

    @Override
    public String toString() {
        return "RouteEntry{" +
                "id=" + id +
                ", city=" + city +
                ", time=" + time +
                ", aPackage=" + aPackage +
                '}';
    }
}
