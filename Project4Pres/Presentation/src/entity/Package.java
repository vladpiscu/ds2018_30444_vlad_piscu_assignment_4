package entity;

public class Package {
    private int id;

    private User sender;
    private User receiver;
    private String name;
    private String description;
    private City senderCity;
    private City receiverCity;
    private Boolean tracking;

    public Package() {
    }

    public Package(User sender, User receiver, String name, String description, City senderCity, City receiverCity, Boolean tracking) {
        this.sender = sender;
        this.receiver = receiver;
        this.name = name;
        this.description = description;
        this.senderCity = senderCity;
        this.receiverCity = receiverCity;
        this.tracking = tracking;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public City getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(City senderCity) {
        this.senderCity = senderCity;
    }

    public City getReceiverCity() {
        return receiverCity;
    }

    public void setReceiverCity(City receiverCity) {
        this.receiverCity = receiverCity;
    }

    public Boolean getTracking() {
        return tracking;
    }

    public void setTracking(Boolean tracking) {
        this.tracking = tracking;
    }

    @Override
    public String toString() {
        return "Package{" +
                "id=" + id +
                ", sender=" + sender +
                ", receiver=" + receiver +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", senderCity=" + senderCity +
                ", receiverCity=" + receiverCity +
                ", tracking=" + tracking +
                '}';
    }
}
