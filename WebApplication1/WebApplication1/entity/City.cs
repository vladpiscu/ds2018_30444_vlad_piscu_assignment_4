﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication1.entity
{
    [Serializable]
    [Table("city")]
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}