﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication1.entity
{
    [Serializable]
    [Table("package")]
    public class Package
    {
        public int Id { get; set; }
        public User Sender { get; set; }
        public User Receiver { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public City SenderCity { get; set; }
        public City ReceiverCity { get; set; }
        public Boolean Tracking { get; set; }
 
        public Package()
        {

        }

        public Package(int id, User sender, User receiver, string name, string description, City senderCity, City receiverCity, Boolean tracking)
        {
            Id = id;
            Sender = sender;
            Receiver = receiver;
            Name = name;
            Description = description;
            SenderCity = senderCity;
            ReceiverCity = receiverCity;
            Tracking = tracking;
        }
    }
}