﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.entity
{
    public class Route
    {
        public List<RouteEntry> RouteList { get; set; }
        public Route()
        {
            RouteList = new List<RouteEntry>();
        }

        public void AddRouteEntry(RouteEntry routeEntry)
        {
            RouteList.Add(routeEntry);
        }
    }
}