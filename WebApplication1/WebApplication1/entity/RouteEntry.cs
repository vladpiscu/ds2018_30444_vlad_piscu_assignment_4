﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication1.entity
{
    [Serializable]
    [Table("route_entry")]
    public class RouteEntry
    {
        public int Id { get; set; }
        public City City { get; set; }
        public DateTime Time { get; set; }
        [Column("aPackage_id")]
        public Package Package { get; set; }
    }
}