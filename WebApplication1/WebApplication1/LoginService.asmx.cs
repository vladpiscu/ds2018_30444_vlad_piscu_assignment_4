﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using WebApplication1.dao;
using WebApplication1.entity;

namespace WebApplication1
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://service/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class LoginService : System.Web.Services.WebService
    {
        UserDao _userDao;
        CityDao _cityDao;
        RouteEntryDao _routeEntryDao;
        PackageDao _packageDao;

        public LoginService()
        {
            _userDao = new UserDao();
            _cityDao = new CityDao();
            _routeEntryDao = new RouteEntryDao();
            _packageDao = new PackageDao();
        }

        [WebMethod]
        public User Login(String username, String password)
        {
            return _userDao.GetByUsernameAndPassword(username, password);
        }

        [WebMethod]
        public User Register(String username, String password)
        {
            User user = new entity.User(username, password, Role.CLIENT);
            _userDao.Add(user);
            _userDao.SaveChanges();
            return user;
        }

        [WebMethod]
        public List<City> GetAllCities()
        {
            return _cityDao.GetAll().ToList();
        }

        [WebMethod]
        public List<Package> GetAllPackagesByUser(int userId)
        {
            var packages = _packageDao.GetAllByUser(userId);
            return packages.ToList();
        }

        [WebMethod]
        public Package SearchPackage(int id)
        {
            return _packageDao.GetById(id);
        }

        [WebMethod]
        public List<RouteEntry> GetRouteForPackage(int packageId)
        {
            return _routeEntryDao.GetRouteByPackage(packageId).ToList();
        }
    }
}
