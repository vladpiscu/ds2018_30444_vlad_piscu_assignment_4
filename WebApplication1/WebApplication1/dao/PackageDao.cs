﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication1.entity;

namespace WebApplication1.dao
{
    public class PackageDao
    {
        protected TrackingContext _dbContext;
        protected DbSet<Package> _dbSet;

        public PackageDao()
        {
            _dbContext = new TrackingContext();
            _dbSet = _dbContext.Packages;
        }

        public void Add(Package package)
        {
            _dbSet.Add(package);
        }

        public void Delete(int key)
        {
            Package packageToDelete = _dbSet.SingleOrDefault(s => s.Id == key);
            Delete(packageToDelete);
        }

        public void Delete(Package package)
        {
            if (_dbContext.Entry(package).State == EntityState.Detached)
            {
                _dbSet.Attach(package);
            }
            _dbSet.Remove(package);
        }

        public IQueryable<Package> GetAll()
        {
            return _dbSet;
        }

        public IQueryable<Package> GetAllByUser(int userId)
        {
            var packages = _dbSet.Include(p => p.Receiver).Include(p => p.Sender).Include(p => p.ReceiverCity).Include(p => p.SenderCity).Where(p => p.Receiver.Id == userId || p.Sender.Id == userId);
            return packages;
        }

        public Package GetById(int id)
        {
            var package = _dbSet.Include(p => p.Receiver).Include(p => p.Sender).Include(p => p.ReceiverCity).Include(p => p.SenderCity).Where(p => p.Id == id).FirstOrDefault();
            return package;
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public void Update(Package package)
        {
            var entity = _dbSet.SingleOrDefault(l => l.Id == package.Id);

            if (entity != null)
            {
                entity = package;
            }
        }

    }
}