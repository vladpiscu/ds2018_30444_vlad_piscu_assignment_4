﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication1.entity;

namespace WebApplication1.dao
{
    public class TrackingContext : DbContext
    {
        public TrackingContext() : base(nameOrConnectionString: "MonkeyFist") { }

        public DbSet<City> Cities { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<RouteEntry> RouteEntries { get; set; }
        public DbSet<User> Users { get; set; }
    }
}