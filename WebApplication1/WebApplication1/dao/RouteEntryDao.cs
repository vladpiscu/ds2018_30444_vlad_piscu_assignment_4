﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication1.entity;

namespace WebApplication1.dao
{
    public class RouteEntryDao
    {
        protected TrackingContext _dbContext;
        protected DbSet<RouteEntry> _dbSet;

        public RouteEntryDao()
        {
            _dbContext = new TrackingContext();
            _dbSet = _dbContext.RouteEntries;
        }

        public IQueryable<RouteEntry> GetAll()
        {
            return _dbSet;
        }

        public IQueryable<RouteEntry> GetRouteByPackage(int packageId)
        {
            var route = _dbSet.Include(p => p.City).Include(p => p.Package).Where(p => p.Package.Id == packageId);
            return route;
        }
    }
}