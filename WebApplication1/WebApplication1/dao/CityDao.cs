﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication1.entity;

namespace WebApplication1.dao
{
    public class CityDao
    {
        protected TrackingContext _dbContext;
        protected DbSet<City> _dbSet;

        public CityDao()
        {
            _dbContext = new TrackingContext();
            _dbSet = _dbContext.Cities;
        }

        public void Add(City city)
        {
            _dbSet.Add(city);
        }

        public void Delete(int key)
        {
            City cityToDelete = _dbSet.SingleOrDefault(s => s.Id == key);
            Delete(cityToDelete);
        }

        public void Delete(City city)
        {
            if (_dbContext.Entry(city).State == EntityState.Detached)
            {
                _dbSet.Attach(city);
            }
            _dbSet.Remove(city);
        }

        public IQueryable<City> GetAll()
        {
            return _dbSet;
        }

        public City GetById(int id)
        {
            var city = _dbSet.Where(p => p.Id == id).FirstOrDefault();
            return city;
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public void Update(City city)
        {
            var entity = _dbSet.SingleOrDefault(l => l.Id == city.Id);

            if (entity != null)
            {
                entity = city;
            }
        }
    }
}