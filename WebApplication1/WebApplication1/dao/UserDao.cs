﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication1.entity;

namespace WebApplication1.dao
{
    public class UserDao
    {
        protected TrackingContext _dbContext;
        protected DbSet<User> _dbSet;

        public UserDao()
        {
            _dbContext = new TrackingContext();
            _dbSet = _dbContext.Users;
        }

        public void Add(User user)
        {
            _dbSet.Add(user);
        }

        public void Delete(int key)
        {
            User userToDelete = _dbSet.SingleOrDefault(s => s.Id == key);
            Delete(userToDelete);
        }

        public void Delete(User user)
        {
            if (_dbContext.Entry(user).State == EntityState.Detached)
            {
                _dbSet.Attach(user);
            }
            _dbSet.Remove(user);
        }

        public IQueryable<User> GetAll()
        {
            return _dbSet;
        }

        public User GetById(int id)
        {
            var user = _dbSet.Where(p => p.Id == id).FirstOrDefault();
            return user;
        }

        public User GetByUsernameAndPassword(string username, string password)
        {
            var user = _dbSet.Where(p => p.Username == username && p.Password == password).FirstOrDefault();
            return user;
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public void Update(User user)
        {
            var entity = _dbSet.SingleOrDefault(l => l.Id == user.Id);

            if (entity != null)
            {
                entity = user;
            }
        }
    }
}