package entity;

import java.util.ArrayList;
import java.util.List;

public class Route {
    private List<RouteEntry> route;

    public Route(){
        route = new ArrayList<>();
    }

    public List<RouteEntry> getRoute() {
        return route;
    }

    public void addRouteEntry(RouteEntry routeEntry){
        route.add(routeEntry);
    }
}
