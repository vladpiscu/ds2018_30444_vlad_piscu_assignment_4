package service;

import dao.CityDao;
import dao.PackageDao;
import dao.RouteEntryDao;
import dao.UserDao;
import entity.City;
import entity.Package;
import entity.RouteEntry;
import entity.User;

import javax.jws.WebService;
import java.sql.Timestamp;
import java.util.List;

@WebService(endpointInterface = "service.IPackageService")
public class PackageService implements IPackageService {
    private PackageDao packageDao;
    private RouteEntryDao routeEntryDao;
    private UserDao userDao;
    private CityDao cityDao;

    public PackageService() {

    }

    public PackageService(PackageDao packageDao, RouteEntryDao routeEntryDao, UserDao userDao, CityDao cityDao) {
        this.packageDao = packageDao;
        this.routeEntryDao = routeEntryDao;
        this.userDao = userDao;
        this.cityDao = cityDao;
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.findUsers();
    }

    @Override
    public User getUserById(int id) {
        return userDao.findUserById(id);
    }

    @Override
    public City getCityById(int id) {
        return cityDao.findCityById(id);
    }

    @Override
    public List<Package> getAllPackages() {
        return packageDao.findPackages();
    }

    @Override
    public Package addPackage(Package aPackage) {
        return packageDao.addPackage(aPackage);
    }

    @Override
    public boolean removePackage(int packageId) {
        return packageDao.deletePackage(packageId);
    }

    @Override
    public Package trackPackage(int packageId) {
        Package aPackage = packageDao.findPackageById(packageId);
        aPackage.setTracking(Boolean.TRUE);
        packageDao.updatePackage(aPackage);
        return aPackage;
    }

    @Override
    public boolean updatePackageRoute(City city, String time, int packageId) {
        Package aPackage = packageDao.findPackageById(packageId);
        Timestamp timestamp = Timestamp.valueOf(time);
        if(aPackage.getTracking() == Boolean.TRUE){
            RouteEntry routeEntry = new RouteEntry(city, timestamp, aPackage);
            routeEntryDao.addRouteEntry(routeEntry);
            return true;
        }
        return false;
    }
}
