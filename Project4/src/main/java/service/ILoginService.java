package service;

import entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ILoginService {
    @WebMethod
    User login(String username, String password);

    @WebMethod
    User register(String username, String password);
}
