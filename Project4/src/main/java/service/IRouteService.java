package service;

import entity.City;
import entity.Package;
import entity.RouteEntry;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@WebService
public interface IRouteService {
    @WebMethod
    Package trackPackage(int packageId);

    @WebMethod
    boolean updatePackageRoute(City city, String time, int packageId);
}
