package service;

import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;
import entity.City;
import entity.Package;
import entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IPackageService {
    @WebMethod
    List<User> getAllUsers();

    @WebMethod
    User getUserById(int id);

    @WebMethod
    City getCityById(int id);

    @WebMethod
    List<Package> getAllPackages();

    @WebMethod
    Package addPackage(Package aPackage);

    @WebMethod
    boolean removePackage(int packageId);

    @WebMethod
    Package trackPackage(int packageId);

    @WebMethod
    boolean updatePackageRoute(City city, String time, int packageId);
}
