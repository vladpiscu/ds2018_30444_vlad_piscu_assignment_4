package service;

import dao.PackageDao;
import dao.RouteEntryDao;
import entity.City;
import entity.Package;
import entity.RouteEntry;

import javax.jws.WebService;
import java.sql.Timestamp;

@WebService(endpointInterface = "service.IRouteService")
public class RouteService implements IRouteService {
    private PackageDao packageDao;
    private RouteEntryDao routeEntryDao;

    public RouteService(PackageDao packageDao, RouteEntryDao routeEntryDao) {
        this.packageDao = packageDao;
        this.routeEntryDao = routeEntryDao;
    }

    public RouteService() {
    }

    @Override
    public Package trackPackage(int packageId) {
        Package aPackage = packageDao.findPackageById(packageId);
        aPackage.setTracking(Boolean.TRUE);
        packageDao.updatePackage(aPackage);
        return aPackage;
    }

    @Override
    public boolean updatePackageRoute(City city, String time, int packageId) {
        Package aPackage = packageDao.findPackageById(packageId);
        Timestamp timestamp = Timestamp.valueOf(time);
        if(aPackage.getTracking() == Boolean.TRUE){
            RouteEntry routeEntry = new RouteEntry(city, timestamp, aPackage);
            routeEntryDao.addRouteEntry(routeEntry);
            return true;
        }
        return false;
    }
}
