package service;

import dao.UserDao;
import entity.Role;
import entity.User;

import javax.jws.WebService;

@WebService(endpointInterface = "service.ILoginService")
public class LoginService implements ILoginService {
    private UserDao userDao;

    public LoginService(){

    }

    public LoginService(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User login(String username, String password) {
        return userDao.findUserByUsernameAndPassword(username, password);
    }

    @Override
    public User register(String username, String password) {
        User user = new User(username, password, Role.CLIENT);
        return userDao.addUser(user);
    }
}
