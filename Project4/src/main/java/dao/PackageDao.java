package dao;

import entity.Package;
import entity.RouteEntry;
import entity.User;
import org.hibernate.*;

import java.util.List;

public class PackageDao {
    private SessionFactory factory;
    RouteEntryDao routeEntryDao;

    public PackageDao(SessionFactory factory, RouteEntryDao routeEntryDao) {
        this.factory = factory;
        this.routeEntryDao = routeEntryDao;
    }

    public Package addPackage(Package aPackage) {
        int packageId = -1;
        Transaction tx = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            packageId = (Integer) session.save(aPackage);
            aPackage.setId(packageId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return aPackage;
    }

    public void updatePackage(Package aPackage) {
        Transaction tx = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            session.update(aPackage);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Package> findPackages() {
        Transaction tx = null;
        List<Package> packages = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            packages = session.createQuery("FROM Package").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return packages;
    }

    @SuppressWarnings("unchecked")
    public Package findPackageById(int id) {
        Transaction tx = null;
        List<Package> packages = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Package WHERE id = :id");
            query.setParameter("id", id);
            packages = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return packages != null && !packages.isEmpty() ? packages.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public List<Package> findPackagesByUser(User user) {
        Transaction tx = null;
        List<Package> packages = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Package WHERE sender = :userId OR receiver = :userId");
            query.setParameter("userId", user);
            packages = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return packages;
    }

    public boolean deletePackage(int id) {
        Transaction tx = null;
        Package aPackage = findPackageById(id);
        if (aPackage == null)
            return false;
        Session session = factory.openSession();
        try {
            List<RouteEntry> routeEntries = routeEntryDao.findRouteEntriesByPackageId(aPackage);
            for(RouteEntry routeEntry : routeEntries)
                routeEntryDao.deleteRouteEntry(routeEntry);
            tx = session.beginTransaction();
            session.delete(aPackage);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
            return false;
        } finally {
            session.close();
        }
        return true;
    }
}
