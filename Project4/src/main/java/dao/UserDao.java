package dao;

import entity.User;
import org.hibernate.*;

import java.util.List;

public class UserDao {

    private SessionFactory factory;

    public UserDao(SessionFactory factory) {
        this.factory = factory;
    }

    public User addUser(User user) {
        int userId = -1;
        Transaction tx = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            userId = (Integer) session.save(user);
            user.setId(userId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return user;
    }

    @SuppressWarnings("unchecked")
    public User findUserByUsernameAndPassword(String username, String password) {
        Transaction tx = null;
        List<User> users = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE (username = :username) AND (password = :password)");
            query.setParameter("username", username);
            query.setParameter("password", password);
            System.out.println(query.toString());
            users = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return users != null && !users.isEmpty() ? users.get(0) : null;
    }

    public List<User> findUsers(){
        Transaction tx = null;
        List<User> users = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            users = session.createQuery("FROM User").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return users;
    }

    @SuppressWarnings("unchecked")
    public User findUserById(int id) {
        Transaction tx = null;
        List<User> users = null;
        Session session = factory.openSession();
        try  {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE id = :id");
            query.setParameter("id", id);
            users = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return users != null && !users.isEmpty() ? users.get(0) : null;
    }
}
