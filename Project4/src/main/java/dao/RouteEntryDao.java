package dao;

import entity.Package;
import entity.RouteEntry;
import org.hibernate.*;

import java.util.List;

public class RouteEntryDao {
    private SessionFactory factory;

    public RouteEntryDao(SessionFactory factory) {
        this.factory = factory;
    }

    public RouteEntry addRouteEntry(RouteEntry routeEntry) {
        int routeEntryId = -1;
        Transaction tx = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            routeEntryId = (Integer) session.save(routeEntry);
            routeEntry.setId(routeEntryId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return routeEntry;
    }

    public boolean deleteRouteEntry(RouteEntry routeEntry) {
        Transaction tx = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            session.delete(routeEntry);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
            return false;
        } finally {
            session.close();
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    public List<RouteEntry> findRouteEntriesByPackageId(Package aPackage) {
        Transaction tx = null;
        List<RouteEntry> routeEntries = null;
        Session session = factory.openSession();
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM RouteEntry WHERE aPackage = :aPackage");
            query.setParameter("aPackage", aPackage);
            routeEntries = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e);
        } finally {
            session.close();
        }
        return routeEntries;
    }
}
