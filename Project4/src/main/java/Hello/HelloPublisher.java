package Hello;

import dao.CityDao;
import dao.PackageDao;
import dao.RouteEntryDao;
import dao.UserDao;
import entity.City;
import entity.Package;
import entity.RouteEntry;
import entity.User;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import service.LoginService;
import service.PackageService;
import service.RouteService;

import javax.xml.ws.Endpoint;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

public class HelloPublisher {
    public static void main(String[] args) {
        /*
        SessionFactory sessionFactory = new Configuration().configure().addAnnotatedClass(Package.class).buildSessionFactory();
        UserDao userDao = new UserDao(sessionFactory);
        CityDao cityDao = new CityDao(sessionFactory);
        PackageDao packageDao = new PackageDao(sessionFactory);
        RouteEntryDao routeEntryDao = new RouteEntryDao(sessionFactory);

        User user1 = userDao.findUserByUsernameAndPassword("c1", "c1");
        User user2 = userDao.findUserByUsernameAndPassword("c2", "c2");
        City city = cityDao.findCityById(1);
        City city2 = cityDao.findCityById(2);
       // Package aPackage = new Package(user1, user2, "Test", "Description", city, city2, Boolean.FALSE);
        //packageDao.addPackage(aPackage);
        List<Package> packages = packageDao.findPackagesByUser(user1);
        System.out.println(packages.get(0).toString());
        RouteEntry routeEntry = new RouteEntry(city, Timestamp.valueOf(LocalDateTime.now()), packages.get(0));
        //routeEntryDao.addRouteEntry(routeEntry);
        List<RouteEntry> routeEntries = routeEntryDao.findRouteEntriesByPackageId(packages.get(0));
        System.out.println(routeEntries.get(0).toString());
        */
        SessionFactory sessionFactory = new Configuration().configure().addAnnotatedClass(Package.class).buildSessionFactory();
        RouteEntryDao routeEntryDao = new RouteEntryDao(sessionFactory);
        PackageDao packageDao = new PackageDao(sessionFactory, routeEntryDao);
        UserDao userDao = new UserDao(sessionFactory);
        CityDao cityDao = new CityDao(sessionFactory);
        Endpoint.publish("http://localhost:7779/ws/packageService", new PackageService(packageDao, routeEntryDao, userDao, cityDao));
        //Endpoint.publish("http://localhost:7779/ws/routeService", new RouteService(packageDao, routeEntryDao));
        //Endpoint.publish("http://localhost:7779/ws/loginService", new LoginService(userDao));
    }
}
