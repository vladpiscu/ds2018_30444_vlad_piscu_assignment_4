package Hello;

import entity.City;
import service.IPackageService;
import service.IRouteService;
import service.RouteService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class HelloClient {
    public static void main(String[] args) throws Exception{
        URL url = new URL("http://localhost:7779/ws/routeService?wsdl");

        //1st argument service URI, refer to wsdl document above
        //2nd argument is service name, refer to wsdl document above
        QName qname1 = new QName("http://service/", "RouteServiceService");
        Service service = Service.create(url, qname1);
        IRouteService hello = service.getPort(IRouteService.class);
        City city = new City("Cluj");
        city.setId(1);
        String s = Timestamp.valueOf(LocalDateTime.now()).toString();
        hello.trackPackage(2);
        System.out.println(hello.updatePackageRoute(city, s, 2));
    }
}
